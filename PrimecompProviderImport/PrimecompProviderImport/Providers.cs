﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;
using PrimecompProviderImport.Models;
using RestSharp;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using BingMapsRESTToolkit;
using Sitecore.Data.Templates;
using Address = PrimecompProviderImport.Models.Address;

namespace PrimecompProviderImport
{
    public static class Providers
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private const string NewLine = "<br />";

        private static readonly string ProviderTemplateId = ConfigurationManager.AppSettings["pcProviderTemplateID"];
        private static readonly string ProviderParentId = ConfigurationManager.AppSettings["pcProviderParentID"];
        private static readonly string AncillaryUrl = ConfigurationManager.AppSettings["AncillaryUrl"];
        private static readonly string PrimecompUrl = ConfigurationManager.AppSettings["PrimecompUrl"];
        private static readonly string BingUrl = ConfigurationManager.AppSettings["bingapi"];

        public static List<Provider> GetAncillaryProviders()
        {
            try
            {
                var client = new RestClient(AncillaryUrl);
                var request = new RestRequest(Method.GET);
                var response = client.Execute(request);
                if (!response.IsSuccessful) return null;

                var provList = JsonConvert.DeserializeObject<List<Provider>>(response.Content);
                return provList;
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred while running Models.Providers.getAncillaryProviders() " + NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                            ex.InnerException + NewLine);
                var m = new Mail()
                {
                    To = To,
                    From = From,
                    Body = "Exception occurred while running Models.Providers.getAncillaryProviders() " + NewLine +
                           " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "PrimecompProviderImport Process Failed"
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    Logger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);

                Console.ReadLine();
                return null;
            }
           
        }
        public static List<Provider> GetPrimeCompProviders()
        {
            try
            {
                
                var client = new RestClient(PrimecompUrl);
                var request = new RestRequest(Method.GET);
                var response = client.Execute(request);
                if (!response.IsSuccessful) return null;

                var provList = JsonConvert.DeserializeObject<List<Provider>>(response.Content);
                return provList;            
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred while running Models.Providers.getPrimeCompProviders() " + NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                            ex.InnerException + NewLine);
                var m = new Mail()
                {
                    To = To,
                    From = From,
                    Body = "Exception occurred while running Models.Providers.getPrimeCompProviders() " + NewLine +
                           " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "PrimecompProviderImport Process Failed"
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    Logger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);

                Console.ReadLine();
                return null;
            }
        }

        public static void ManageProvider (Provider provider)
        {
            string strNewProvider = "";
            try
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    var db = Sitecore.Configuration.Factory.GetDatabase("master");
                    var providerTemplate = db.GetTemplate(ProviderTemplateId.ToString());
                    var parentItem = db.GetItem(ProviderParentId);
                    strNewProvider = provider.ID.ToString();
                    var newProvider = db.GetItem(parentItem.Paths.Path + "/" + strNewProvider);

                    if (newProvider == null || newProvider.Empty)
                    {
                        // New Provider
                        newProvider = parentItem.Add(strNewProvider, providerTemplate);
                        Console.WriteLine("New Provider: " + provider.ID.ToString());
                        Logger.Info("New Provider: " + provider.ID.ToString());
                    }
                    else
                    {
                        Console.WriteLine("Existing Provider: " + provider.ID.ToString());
                        Logger.Info("Existing Provider: " + provider.ID.ToString());
                    }
                    Console.WriteLine("Updating Provider: " + provider.ID.ToString());
                    UpdateProvider(newProvider, provider);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("PrimecompProviderImport :: ImportProviders -> Exception : " + ex.ToString() + "PROVIDER " + strNewProvider);
                Console.WriteLine(ex.Message);
            }
        }
        
        private static void UpdateProvider(Item item, Provider provider)
        {
            item.Editing.BeginEdit();
            try
            {
                item.Fields["FirstName"].Value = (provider.FirstName != null) ? provider.FirstName.ToString() : null;
                item.Fields["MiddleName"].Value = (provider.MiddleName != null) ? provider.MiddleName.ToString() : null;
                item.Fields["LastName"].Value = (provider.LastName != null) ? provider.LastName.ToString() : null;
                item.Fields["Specialty"].Value = (provider.Specialty != null) ? provider.Specialty.ToString() : null;
                item.Fields["SpecialtyNotes"].Value = (provider.SpecialtyNotes != null) ? provider.SpecialtyNotes.ToString() : null;
                item.Fields["SubSpecialty"].Value = (provider.SubSpecialty != null) ? provider.SubSpecialty.ToString() : null;
                item.Fields["Languages"].Value = (provider.Language != null) ? provider.Language.ToString() : null;
                item.Fields["Gender"].Value = (provider.Gender != null) ? provider.Gender.ToString() : null;
                item.Fields["Email"].Value = (provider.Email != null) ? provider.Email.ToString() : null;
                item.Fields["ProviderType"].Value = (provider.ProviderType != null) ? provider.ProviderType.ToString() : null;
                item.Fields["Degree"].Value = (provider.Degree != null) ? provider.Degree.ToString() : null;
                item.Fields["GroupName"].Value = (provider.GroupName != null) ? provider.GroupName.ToString() : null;
                item.Fields["Site"].Value = (provider.site != null) ? provider.site.ToString() : null;
                if (provider.LocationInformation != null)
                {
                    item.Fields["County"].Value = (provider.LocationInformation.County != null) ?
                        provider.LocationInformation.County.ToString() : null;
                    item.Fields["Address"].Value = (provider.LocationInformation.PhysicalAddress != null) ? 
                        provider.LocationInformation.PhysicalAddress.ToString() : null;
                    item.Fields["City"].Value = (provider.LocationInformation.City != null) ? 
                        provider.LocationInformation.City.ToString() : null;
                    item.Fields["State"].Value = (provider.LocationInformation.State != null) ? 
                        provider.LocationInformation.State.ToString() : null;
                    item.Fields["Zip"].Value = (provider.LocationInformation.Zip != null) ? 
                        provider.LocationInformation.Zip.ToString() : null;
                    item.Fields["Phone"].Value = (provider.LocationInformation.Phone != null) ? 
                        provider.LocationInformation.Phone.ToString() : null;
                    item.Fields["Fax"].Value = (provider.LocationInformation.Fax != null) ? 
                        provider.LocationInformation.Fax.ToString() : null;
                }
                if (provider.AdditionalFacilityAddress != null)
                {
                    item.Fields["AdditionalFacilityAddress"].Value = (provider.AdditionalFacilityAddress.PhysicalAddress != null) ?
                        provider.AdditionalFacilityAddress.PhysicalAddress.ToString() : null;
                    item.Fields["AdditionalFacilityCity"].Value = (provider.AdditionalFacilityAddress.City != null) ? 
                        provider.AdditionalFacilityAddress.City.ToString() : null;
                    item.Fields["AdditionalFacilityState"].Value = (provider.AdditionalFacilityAddress.State != null) ?
                        provider.AdditionalFacilityAddress.State.ToString() : null;
                    item.Fields["AdditionalFacilityZip"].Value = (provider.AdditionalFacilityAddress.Zip != null) ?
                        provider.AdditionalFacilityAddress.Zip.ToString() : null;
                    item.Fields["AdditionalFacilityPhone"].Value = (provider.AdditionalFacilityAddress.Phone != null) ?
                        provider.AdditionalFacilityAddress.Phone.ToString() : null;
                    item.Fields["AdditionalFacilityFax"].Value = (provider.AdditionalFacilityAddress.Fax != null) ?
                        provider.AdditionalFacilityAddress.Fax.ToString() : null;
                }
                if (provider.SecondaryFacilityAddress != null)
                {
                    item.Fields["SecondaryFacilityAddress"].Value = (provider.SecondaryFacilityAddress.PhysicalAddress != null) ?
                        provider.SecondaryFacilityAddress.PhysicalAddress.ToString() : null;
                    item.Fields["SecondaryFacilityCity"].Value = (provider.SecondaryFacilityAddress.City != null) ?
                        provider.SecondaryFacilityAddress.City.ToString() : null;
                    item.Fields["SecondaryFacilityState"].Value = (provider.SecondaryFacilityAddress.State != null) ?
                        provider.SecondaryFacilityAddress.State.ToString() : null;
                    item.Fields["SecondaryFacilityZip"].Value = (provider.SecondaryFacilityAddress.Zip != null) ?
                        provider.SecondaryFacilityAddress.Zip.ToString() : null;
                    item.Fields["SecondaryFacilityPhone"].Value = (provider.SecondaryFacilityAddress.Phone != null) ?
                        provider.SecondaryFacilityAddress.Phone.ToString() : null;
                    item.Fields["SecondaryFacilityFax"].Value = (provider.SecondaryFacilityAddress.Fax != null) ?
                        provider.SecondaryFacilityAddress.Fax.ToString() : null;
                }
                item.Fields["EntryDate"].Value = DateUtil.ToIsoDate(DateTime.Now);
                item.Fields["EntryProcess"].Value = "PrimecompProviderImport.UpdateProvider";
                if ((string.IsNullOrEmpty(item.Fields["lat"].Value)||item.Fields["lat"].ToString()=="0")&&provider.LocationInformation!=null&&
                    !string.IsNullOrEmpty(provider.LocationInformation.PhysicalAddress))
                {
                    var address = new Address();
                    address.StreetAddress = item.Fields["Address"].Value;
                    address.City = item.Fields["City"].Value;
                    address.State = item.Fields["State"].Value;
                    address.Zip = item.Fields["Zip"].Value;
                    var zip = GetLatLong(address);
                    if (zip != null && zip.lat > 0)
                    {
                        item.Fields["lat"].Value = zip.lat.ToString();
                        item.Fields["long"].Value = zip.lon.ToString();
                    }
                }

                item.Editing.AcceptChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                Logger.Error("PrimecompProviderImport :: UpdateProvider -> Could not update item " + item.Paths.FullPath + item.Name + ": " + ex.ToString());
                Console.WriteLine("PrimecompProviderImport :: UpdateProvider -> Could not update item " + item.Paths.FullPath + item.Name + ": " + ex.ToString());
                item.Editing.CancelEdit();
            }
            finally
            {
                item.Editing.EndEdit();
            }
        }
        private static ZipCode GetLatLong(Address address)
        {
            var zip = new ZipCode();
            try
            {
                if (address != null && !string.IsNullOrEmpty(address.StreetAddress) && !string.IsNullOrEmpty(address.City))
                {
                    var client = new RestClient(BingUrl);
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Address", JsonConvert.SerializeObject(address));
                    var response = client.Execute(request);
                    if (!response.IsSuccessful) return null;

                    try
                    {
                        var coords = JsonConvert.DeserializeObject<Coordinate>(response.Content);
                        zip.lat = coords.Latitude;
                        zip.lon = coords.Longitude;
                    }
                    catch (Exception e)
                    {
                        Logger.Error("There was an error getting Lat/Long from Microservice " + e.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("PrimecompProviderImport :: GetLatLong -> Could not obtain coordinates " + ex.Message);
            }
            return zip;
        }
    }
}
