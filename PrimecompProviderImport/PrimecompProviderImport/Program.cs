﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NLog;
using PrimecompProviderImport.Models;

namespace PrimecompProviderImport
{

    class Program
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private const string NewLine = "<br />";

        static void Main(string[] args)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") {FileName = "PrimecompProviderImport.log"};
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {   
                Console.WriteLine("Starting import from " + ConfigurationManager.ConnectionStrings["master"].ConnectionString);

                //Run Process
                RunProcess();
            }
            catch (Exception ex)
            {
                Logger.Info("Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " + 
                            ex.InnerException + NewLine);
                var m = new Mail
                {
                    To = To,
                    From = From,
                    Body = "Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + ex.Message +
                           NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "PrimecompProviderImport Process Failed"
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    Logger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
            }
        }
        private static void RunProcess()
        {
            try
            {
                //Start the Import
                Console.WriteLine("Import started...");
                Logger.Info("Import started......");
                //Providers _objProviders = new Providers();
                //_objProviders.GetProviderData();
                
                Console.WriteLine("Getting Provider from Sharepoint..........");
                Logger.Info("Getting Provider from Sharepoint..........");
                var providers = Providers.GetPrimeCompProviders();//_objProviders.getPrimeCompProviders();
                Console.WriteLine(providers.Count + " Physicians found. Processing Started...");
                Logger.Info(providers.Count + " Physicians found. Processing Started...");
                foreach (var provider in providers.Where(provider => !string.IsNullOrEmpty(provider.ID)))
                {
                    Console.WriteLine("Processing Provider: " + provider.ID);
                    Logger.Info("Processing Provider: " + provider.ID);
                    Providers.ManageProvider(provider);
                    //_objProviders.ManageProvider(provider);
                    Console.WriteLine("Finished adding Provider: " + provider.ID);
                    Logger.Info("Finished adding Provider: " + provider.ID);
                }

                var Ancillary = Providers.GetAncillaryProviders();// _objProviders.getAncillaryProviders();
                Console.WriteLine(Ancillary.Count + " Ancillary found. Processing Started...");
                Logger.Info(Ancillary.Count + " Ancillary found. Processing Started...");
                foreach (var provider in Ancillary.Where(provider => !string.IsNullOrEmpty(provider.ID)))
                {
                    Console.WriteLine("Processing Ancillary Provider: " + provider.ID);
                    Logger.Info("Processing Ancillary Provider: " + provider.ID);
                    //_objProviders.ManageProvider(provider);
                    Providers.ManageProvider(provider);
                    Console.WriteLine("Finished adding Ancillary Provider: " + provider.ID);
                    Logger.Info("Finished adding Ancillary Provider: " + provider.ID);
                }
                Console.WriteLine("Import complete!");
                Logger.Info("Import complete!");

                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred while running Program.RunProcess() " + NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                             ex.InnerException + NewLine);
                var m = new Mail
                {
                    To = To,
                    From = From,
                    Body = "Exception occurred while running Program.RunProcess() " + NewLine + " ex.Message: " +
                           ex.Message + NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "PrimecompProviderImport Process Failed"
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    Logger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
                Logger.Error("BHPrimecompImport Agent :: Program Main Exception StackTrace: " + ex.StackTrace);
            }


        }
    }
}
